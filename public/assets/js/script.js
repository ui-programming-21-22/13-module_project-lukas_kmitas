const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

const scale = 2;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
const waterLoop = [0,1,2,3];
const fireLoop = [0,1,2,3,4,5,6];
const frameLimit = 7;
const foodWidth = 32;
const foodHeight = 32;
const forestWidth = 400;
const forestHeight = 320;

const username = localStorage.getItem('username');
const score = localStorage.getItem('score');
const timerClock = localStorage.getItem('time');

let gameOver = 0;
let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 2;

let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);
let randomFoodX = Math.abs(Math.floor(Math.random() * 7));
let randomFoodXSelect = randomFoodX * 64;
let randomFoodY = Math.abs(Math.floor(Math.random() * 4));
let randomFoodYSelect = randomFoodY * 64;

let randomFireX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomFireY = Math.abs(Math.floor(Math.random() * 799) - 50);

let scoreCount = 0;
let timerCount = 0;

var fireMeter = 200;
let fireOn = 0;
let fireTimer = 0;
let fireTimerLimit = 200;

var waterMeter = 200;
let waterOn = 0;
let scaleOn = 0;

let resetTimer = 0;
let resetTimerInterval = 300;
let resetBool = 0;
//let fireArr = [];
//let number = 5;
//Array.push(number); 

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

if (score)
{
    scoreCount = score;
}

if (timerClock)
{
    timerCount = timerClock;
}

function addName() 
{
    let header = document.getElementById("main-header");
    header.innerHTML = "Username: " + username;
}

addName();

function randoPos(rangeX, rangeY, delta)
{
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}

function newFood()
{
    randomFoodX = Math.abs(Math.floor(Math.random() * 7));
    randomFoodXSelect = randomFoodX * 64;
    randomFoodY = Math.abs(Math.floor(Math.random() * 4));
    randomFoodYSelect = randomFoodY * 64;
    foodPosition = new randoPos(1099, 499, 50);
}

function newFire()
{
   firePosition = new randoPos(1099, 699, 50);
}

let gameOverSprite = new Image();
gameOverSprite.src = "assets/img/GameOver_skull.png";

let forestSprite = new Image();
forestSprite.src = "assets/img/forest.png"

let character = new Image();
character.src = "assets/img/Green-16x18-spritesheet.png";

let foodSprite = new Image();
foodSprite.src = "assets/img/fruitnveg64wh37.png";

let pondSprite = new Image();
pondSprite.src = "assets/img/Lake.png";

let waterSprite = new Image();
waterSprite.src = "assets/img/pourWater.png";

let fireSprite = new Image();
fireSprite.src = "assets/img/Fire.png"
let fireSprite2 = new Image();
fireSprite2.src = "assets/img/Fire.png"
let fireSprite3 = new Image();
fireSprite3.src = "assets/img/Fire.png"
let fireSprite4 = new Image();
fireSprite4.src = "assets/img/Fire.png"

// GameObject holds positional information
// Can be used to hold other information based on requirements
function GameObject(spritesheet, x, y, width, height) 
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}

// Default Player
let player = new GameObject(character, 500, 250, 200, 200);
let foods = new GameObject(foodSprite, randomX, randomY, 100, 100);
//let water = new GameObject(waterSprite, 300, 250, 100, 100);
//let fire = new GameObject(fireSprite, randomFireX, randomFireY, 100, 100);

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) 
{
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) 
{
    // Take Input from the Player
    // console.log("Input");
    // console.log("Event type: " + event.type);
    //console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") 
    {
        switch (event.keyCode) 
        {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            case 32:
                gamerInput = new GamerInput("Space");
                break;
            case 82:
                gamerInput = new GamerInput("R");
                break;
            case 83:
                speed = 4;
                break;
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } 
    else 
    {
        gamerInput = new GamerInput("None");
        speed = 2;
    }
}


var dynamic = nipplejs.create({
    color: 'grey',
});

dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
       //console.log("direction up");
       gamerInput = new GamerInput("Up");
    });
    nipple.on('dir:down', function (evt, data) {
        //console.log("direction down");
        gamerInput = new GamerInput("Down");
     });
     nipple.on('dir:left', function (evt, data) {
        //console.log("direction left");
        gamerInput = new GamerInput("Left");
     });
     nipple.on('dir:right', function (evt, data) {
        //console.log("direction right");
        gamerInput = new GamerInput("Right");
     });
     nipple.on('hold', function (evt, data) {
         GamerInput = new GamerInput("Space");
     })
     nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new GamerInput("None");
     });
});

function update() {
    // console.log("Update");
    // Check Input
    timerCount++;
    if (gamerInput.action === "Up") 
    {
        if (player.y < 200)
        {
            console.log("player at top edge");
        }
        else
        {
            player.y -= speed; // Move Player Up
        }
        currentDirection = 1;
    } 
    else if (gamerInput.action === "Down") 
    {
        if (player.y + scaledHeight > canvas.height)
        {
            console.log("player at bottom edge");
        }
        else
        {
            player.y += speed; // Move Player Down
        }
        currentDirection = 0;
    } 
    else if (gamerInput.action === "Left") 
    {
        if (player.x < 0)
        {
            console.log("player at left edge");
        }
        else
        {
            player.x -= speed; // Move Player Left
        }
        currentDirection = 2;
    } 
    else if (gamerInput.action === "Right") 
    {
        if (player.x + scaledWidth > canvas.width)
        {
            console.log("player at Right edge");
        }
        else
        {
            player.x += speed; // Move Player Right
        }
        currentDirection = 3;
    }
    else if (gamerInput.action === "Space")
    {
        if(waterMeter >= 0)
        {
            waterMeter -= 10;
            waterOn = 1;
        }
    }
    else if (gamerInput.action === "R")
    {
        refillWater();
    }
    else if (gamerInput.action === "None")
    {
        refillWater();
        waterOn = 0;
    }
    localStorage.setItem("time", timerCount);
    if (foodPosition.y < 200 || foodPosition.y > 500)
    {
        newFood();
    }
    if (firePosition.y < 200 || firePosition.y > 500)
    {
        newFire();
    }
    if(fireOn == 0)
    {
        fireMeter += 0.1
        if(fireMeter >= 200)
        {
            fireMeter = 200;
        }
        if (fireMeter <= 0)
        {
            fireMeter = 0;
            gameOver = 1;
        }
    }
    fireCollisions();
    rockCollisions();
    reset();
}

function rockCollisions()
{
    if (player.x > 340)
    {
         //player.x = 340;
    }
    if(player.y > 570)
    {
        player.y = 570;
    }
    if (270 < player.x + scaledWidth && //collision from left to right
        340 + width > player.x && // collision from right to left
        480 < player.y + scaledHeight && // collision from top to bottom
        480 + height > player.y // collision from bottom to top
        )
        {
            console.log("Rock collision!");
            speed = -1;
        }
    else 
    {
        speed = 2;
    } 

}

/// To refill water bucket
function refillWater()
{
    if(player.x > 540 && player.x < 680 &&
        player.y > 410  && player.y < 520)
        {
            waterMeter += 200;
            if(waterMeter > 200)
            {
                waterMeter = 200;
            }
        }
}

function drawFrame(image, frameX, frameY, canvasX, canvasY) 
{
    context.drawImage(image,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}

let scaleF = 1;
const widthF = 66;
const heightF = 44;
let scaledWidthF = scaleF * widthF;
let scaledHeightF = scaleF * heightF;
function drawFireFrame(image, frameX, frameY, canvasX, canvasY) 
{
    context.drawImage(image,
                  frameX * widthF, frameY * heightF, widthF, heightF,
                  canvasX, canvasY, scaledWidthF, scaledHeightF);
}

const scaleW = 1;
const widthW = 54;
const heightW = 40;
const scaledWidthW = scaleW * widthW;
const scaledHeightW = scaleW * heightW;
function drawWaterFrame(image, frameX, frameY, canvasX, canvasY) 
{
    context.drawImage(image,
                  frameX * widthW, frameY * heightW, widthW, heightW,
                  canvasX, canvasY, scaledWidthW, scaledHeightW);
}

function animate() 
{
    if (gamerInput.action != "None")
    {
        frameCount++;
        if (frameCount >= frameLimit)
         {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) 
            {
                currentLoopIndex = 0;
            }
        }      
    }
    else
    {
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

foodPosition = new randoPos(1099, 499, 50);

function manageFood()
{
    // place the piece of food
    context.drawImage(foodSprite, randomFoodX*64, randomFoodY*64, 64, 64,
         foodPosition.x, foodPosition.y, foodWidth, foodHeight);
    // check for collision (eating)

    //if (rect1.x < rect2.x + rect2.width && 
    //    rect1.x + rect1.width > rect2.x && 
    //    rect1.y < rect2.y + rect2.height &&
    //    rect1.y + rect1.height > rect2.y) {
    // collision detected!
    //}
    if (foodPosition.x < player.x + scaledWidth && //collision from left to right
        foodPosition.x + foodWidth > player.x && // collision from right to left
        foodPosition.y < player.y + scaledHeight && // collision from top to bottom
        foodPosition.y + foodHeight > player.y // collision from bottom to top
        )
    {
        console.log("collision!");
        if (gameOver == 0)
        {
            scoreCount++;
        }
        localStorage.setItem("score", scoreCount);
        newFood();
    }
}

function fireCollisions()
{
    if(waterOn == 1)
    {
        if (firePosition.x < player.x + scaledWidthW + 40 && //collision from left to right
        firePosition.x + widthF + 40 > player.x && // collision from right to left
        firePosition.y < player.y + scaledHeightW + 40 && // collision from top to bottom
        firePosition.y + heightF + 40 > player.y // collision from bottom to top
        )
        {
            console.log("Water collision!");
            newFire();
            fireMeter += 30;
            fireOn = 0;
        }
    }
}

function writeScore()
{
    context.fillStyle = "#00FF00";
    let scoreString = "score: " + scoreCount;
    context.font = '22px sans-serif';
    context.fillText(scoreString, 1000, 20)
}

function writeTimer()
{
    context.fillStyle = "#00FF00";
    let timerString = "Time: " + timerCount;
    context.font = '22px sans-serif';
    context.fillText(timerString, 700, 20);
}

function writeString()
{
    context.fillStyle = "#00FFFF"
    let fireMeterText = "Fire Meter";
    context.font = '22px sans-serif';
    context.fillText(fireMeterText, 500, 20);
}

function draw() 
{
    context.clearRect(0,0, canvas.width, canvas.height);
    if (gameOver == 0)
    {
        drawFirebar();
        drawBucketBar();
        drawPondSprite();
        manageFood();
        drawFireSprite();
        animate();
        drawWaterSprite();
        writeScore();
        writeTimer();
        writeString();
        drawUI();
    }
    else if (gameOver == 1)
    {
        writeScore();
        writeTimer();
        drawGameOver();
    }
}

function reset()
{
    if (gameOver == 1)
    {
        if(resetBool == 1)
        {
            scoreCount = 0;
            timerCount = 0;
            fireOn = 0;
            gameOver = 0;
            fireTimer = 0;
            fireMeter = 200;
            waterMeter = 200;
            resetBool = 0;
        }
        else if (resetTimer >= resetTimerInterval)
        {
            resetTimer = 0;
            resetBool = 1;
        }
        else
        {
            resetTimer++;
        }
    }
}

function drawUI()
{
}

function drawPondSprite()
{
    context.drawImage(pondSprite, 0, 0, 140, 139,
         550, 410, 116, 115);
}

function drawFirebar()
{
    var width = 200;
    var height = 20;
    var max = 200;
  
    context.fillStyle = "#000000";
    context.clearRect(450, 2, canvas.width, canvas.height);
    context.fillRect(450, 2, width, height);
  
    context.fillStyle = "#00d600";
    var fillVal = Math.min(Math.max(fireMeter / max, 0), 1);
    context.fillRect(450, 2, fillVal * width, height);

    if(fireMeter <= 0)
    {
        gameOver = 1;
    }
}

function drawBucketBar()
{
    var width = 30;
    var height = 200;
    var max = 200;
  
    context.fillStyle = "#000000";
    context.clearRect(5, 25, canvas.width, canvas.height);
    context.fillRect(5, 25, width, height);
    
   context.fillStyle = "#5757ff";
   var fillVal = Math.min(Math.max(0, waterMeter / max), 1);
   context.fillRect(5, 25, width, fillVal * height);

}

function drawForest()
{
    context.drawImage(forestSprite, 0, 0);
}

let currentFireLoopIndex = 0;
firePosition = new randoPos(1099, 799, 50);

function drawFireSprite()
{ 
    if (fireOn == 1)
    {
        frameCount++;
        if (frameCount >= frameLimit)
        {
            frameCount = 0;
            currentFireLoopIndex++;
            if (currentFireLoopIndex >= fireLoop.length) 
            {
              currentFireLoopIndex = 0;
            }
        }     
        //drawFireFrame(fireSprite, fireLoop[currentFireLoopIndex], 0, 300, 100);   

        for (let i = 0; i < 5; i++)
        {
            if (scoreCount  >= 2)
            {
                scaleF = 1;
                drawFireFrame(fireSprite, fireLoop[currentFireLoopIndex], 0, firePosition.x, firePosition.y); 
            }
            else if(scoreCount <= 3)
            {
                scaleF = 5;
                drawFireFrame(fireSprite, fireLoop[currentFireLoopIndex], 0, firePosition.x, firePosition.y); 
            }
            //drawFireFrame(fireSprite, fireLoop[currentFireLoopIndex], 0, firePosition.x, firePosition.y); 
            //drawFireFrame(fireSprite2, fireLoop[currentFireLoopIndex], 0, firePosition.x, firePosition.y); 
            //drawFireFrame(fireSprite3, fireLoop[currentFireLoopIndex], 0, firePosition.x, firePosition.y); 
            //drawFireFrame(fireSprite4, fireLoop[currentFireLoopIndex], 0, firePosition.x, firePosition.y);
        }
        fireMeter -= 0.1;
    }
    else if(fireTimer >= fireTimerLimit)
    {
        fireOn = 1;
        fireTimer = 0;
    }
    else
    {
        fireTimer++
    }
}

let currentWaterLoopIndex = 0
const frameWaterLimit = 4;

function drawWaterSprite()
{
    if(waterOn == 1)
    {
        frameCount++;
        if (frameCount >= frameWaterLimit)
        {
            frameCount = 0;
            currentWaterLoopIndex++;
            if (currentWaterLoopIndex >= waterLoop.length) 
            {
                currentWaterLoopIndex = 0;
            }
        } 
        if(currentDirection == 0)
        {
            drawWaterFrame(waterSprite, waterLoop[currentWaterLoopIndex], 0, player.x, player.y + 50);   
        }
        if (currentDirection == 1)
        {    
            drawWaterFrame(waterSprite, waterLoop[currentWaterLoopIndex], 0, player.x, player.y - 50);      
        }
        if (currentDirection == 2)
        {
            drawWaterFrame(waterSprite, waterLoop[currentWaterLoopIndex], 0, player.x - 50, player.y);  
        }
        if (currentDirection == 3)
        {
            drawWaterFrame(waterSprite, waterLoop[currentWaterLoopIndex], 0, player.x + 50, player.y);  
        }
    }
}

function drawGameOver()
{
    context.drawImage(gameOverSprite, 0, 0, 5000, 5000,
        300, 100, 500, 500);
    //console.log("GAME OVER!");
}

function gameloop() 
{
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);
